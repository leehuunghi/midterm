﻿// quanlychitieu.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "quanlychitieu.h"
#include <windowsx.h>
#include <Winuser.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;
#define MAX_LOADSTRING 100
// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_QUANLYCHITIEU, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_QUANLYCHITIEU));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_QUANLYCHITIEU));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_QUANLYCHITIEU);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;
   
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, L"Quản lý chi tiêu", WS_OVERLAPPEDWINDOW,
      300, 10, 600, 700, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	{
					  INITCOMMONCONTROLSEX icex;

					  // Ensure that the common control DLL is loaded. 
					  icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
					  icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_DATE_CLASSES;
					  InitCommonControlsEx(&icex);

					  //init font
					  LOGFONT lf;
					  GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
					  HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
						  lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						  lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						  lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						  lf.lfPitchAndFamily, lf.lfFaceName);
					  hFont = CreateFont(20, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
						  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						  DEFAULT_PITCH | FF_DONTCARE, TEXT("Segoe UI"));
					  HFONT hFont1 = CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, 
						  ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Segoe UI Bold"));
					  HFONT hFont2 = CreateFont(25, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET,
						  OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						  DEFAULT_PITCH | FF_DONTCARE, TEXT("Segoe UI"));

					  //init control
					  g_hTitle = CreateWindowEx(0, L"STATIC", L"QUẢN LÝ CHI TIÊU",
						  WS_CHILD | WS_VISIBLE,
						  170, 10, 350, 50, hWnd, NULL, hInst, NULL);
					  HWND h1 = CreateWindowEx(0, L"BUTTON", L"Thêm chi tiêu",
						  BS_GROUPBOX | WS_CHILD | WS_VISIBLE,
						  10, 50, 570, 100, hWnd, NULL, hInst, NULL);
					  HWND h2 = CreateWindowEx(0, L"BUTTON", L"Danh sách chi tiêu",
						  BS_GROUPBOX | WS_CHILD | WS_VISIBLE,
						  10, 170, 570, 200, hWnd, NULL, hInst, NULL);
					  h3 = CreateWindowEx(0, L"BUTTON", L"Thống kê chi tiêu",
						  BS_GROUPBOX | WS_CHILD | WS_VISIBLE,
						  10, 380, 570, 260, hWnd, NULL, hInst, NULL);

					  SetBkColor(GetDC(h1), RGB(255, 255, 255));

					  HWND h4 = CreateWindowEx(0, L"STATIC", L"Loại", WS_CHILD | WS_VISIBLE,
						  20, 75, 150, 50, hWnd, NULL, hInst, NULL);
					  HWND h5 = CreateWindowEx(0, L"STATIC", L"Nội dung", WS_CHILD | WS_VISIBLE,
						  150, 75, 150, 50, hWnd, NULL, hInst, NULL);
					  HWND h6 = CreateWindowEx(0, L"STATIC", L"Số tiền (VNĐ)", WS_CHILD | WS_VISIBLE,
						  300, 75, 150, 50, hWnd, NULL, hInst, NULL);
					  HWND h7 = CreateWindowEx(0, L"STATIC", L"Ngày", WS_CHILD | WS_VISIBLE,
						  450, 75, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h8 = CreateWindowEx(0, L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE,
						  70, 448, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h9 = CreateWindowEx(0, L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE,
						  70, 478, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h10 = CreateWindowEx(0, L"STATIC", L"Nhà cửa", WS_CHILD | WS_VISIBLE,
						  70, 508, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h11 = CreateWindowEx(0, L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE,
						  70, 538, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h12 = CreateWindowEx(0, L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE,
						  70, 568, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h13 = CreateWindowEx(0, L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE,
						  70, 598, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hCombobox = CreateWindowEx(0, L"COMBOBOX", L"Loại", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST,
						  20, 100, 100, 1000, hWnd, NULL, hInst, NULL);
					  g_hNote = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE|WS_TABSTOP|ES_AUTOVSCROLL|ES_AUTOHSCROLL|WS_TABSTOP,
						  150, 100, 80, 30, hWnd, NULL, hInst, NULL);
					  g_hMoney = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | ES_NUMBER | ES_AUTOHSCROLL | ES_RIGHT|WS_TABSTOP,
						  300, 100, 80, 30, hWnd, NULL, hInst, NULL);
					  g_hButtonCacul = CreateWindowEx(0, L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_OWNERDRAW,
						 230, 135, 100, 30, hWnd, (HMENU)ID_BUTTONINSERT, hInst, NULL);
					  g_hDate = CreateWindowEx(0, DATETIMEPICK_CLASS, TEXT("DateTime"),
						   WS_CHILD | WS_VISIBLE,
						  450, 100, 100, 30, hWnd, NULL, hInst, NULL);
					  g_hListView = CreateWindowEx(0, WC_LISTVIEW, L"",
						  WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT|LVS_ALIGNTOP ,
						  20, 190, 550, 170, hWnd, (HMENU)ID_LISTVIEW, hInst, NULL);
						
					  g_hNumber.resize(7);
					  g_hNumber[0] = CreateWindowEx(0, L"STATIC", L"0%", WS_CHILD | WS_VISIBLE,
						  180, 448, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hNumber[1] = CreateWindowEx(0, L"STATIC", L"0%", WS_CHILD | WS_VISIBLE,
						  180, 478, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hNumber[2] = CreateWindowEx(0, L"STATIC", L"0%", WS_CHILD | WS_VISIBLE,
						  180, 508, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hNumber[3] = CreateWindowEx(0, L"STATIC", L"0%", WS_CHILD | WS_VISIBLE,
						  180, 538, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hNumber[4] = CreateWindowEx(0, L"STATIC", L"0%", WS_CHILD | WS_VISIBLE,
						  180, 568, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hNumber[5] = CreateWindowEx(0, L"STATIC", L"0%", WS_CHILD | WS_VISIBLE,
						  180, 598, 100, 50, hWnd, NULL, hInst, NULL);
					  g_hSumMoney = CreateWindowEx(0, L"STATIC", L"0", WS_CHILD | WS_VISIBLE|ES_CENTER,
						  370, 405, 100, 50, hWnd, NULL, hInst, NULL);
					  HWND h14 = CreateWindowEx(0, L"STATIC", L"Tổng tiền", WS_CHILD | WS_VISIBLE,
						 290, 410, 80, 20, hWnd, NULL, hInst, NULL);
					  HWND h15 = CreateWindowEx(0, L"STATIC", L"VNĐ", WS_CHILD | WS_VISIBLE,
						  470, 410, 80, 20, hWnd, NULL, hInst, NULL);
					  //set font
					  SendMessage(g_hDate, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hListView, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hButtonCacul, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hMoney, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hCombobox, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hNote, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hTitle, WM_SETFONT, WPARAM(hFont1), TRUE);
					  SendMessage(h1, WM_SETFONT, WPARAM(hFont2), TRUE);
					  SendMessage(h2, WM_SETFONT, WPARAM(hFont2), TRUE);
					  SendMessage(h3, WM_SETFONT, WPARAM(hFont2), TRUE);
					  SendMessage(h4, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h5, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h6, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h7, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h8, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h9, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h10, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h11, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h12, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h13, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h14, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(h15, WM_SETFONT, WPARAM(hFont), TRUE);
					  SendMessage(g_hSumMoney, WM_SETFONT, WPARAM(hFont2), TRUE);
					  for (int i = 0; i < 6; i++) SendMessage(g_hNumber[i], WM_SETFONT, WPARAM(hFont), TRUE);
					  //insert data to combobox
					  for (int i = 0; i < 6; i++)
					  {
						  SendMessage(g_hCombobox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)LoaiChiTieu[i]);
					  }
					  //init col list view
					  init4col();
					  

					  GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

					  //xử lý load dữ liệu
					  LoadData();
					  for (int i = 0; i < g_vData.size(); i++)
					  {
						  InsertItemToListView(g_vData[i]->type, g_vData[i]->note, g_vData[i]->money, g_vData[i]->date);
					  }
					  InvalidateRect(hWnd, NULL, TRUE);
					  ComboBox_SetCurSel(g_hCombobox, 0);
					  break;
	}
	case WM_CTLCOLORSTATIC:
	{
							  if ((HWND)lParam == g_hTitle || (HWND)lParam == g_hNumber[0] || (HWND)lParam == g_hNumber[1]
								  || (HWND)lParam == g_hNumber[2] || (HWND)lParam == g_hNumber[3] || (HWND)lParam == g_hNumber[4]
								  || (HWND)lParam == g_hNumber[5] || (HWND)lParam == g_hNumber[6])
							  {
								  SetTextColor((HDC)wParam, RGB(108, 184, 35));  // thay đổi màu chữ sang màu xanh
								  SetBkMode((HDC)wParam, TRANSPARENT);		// không có màu nền của chữ
								  return (LRESULT)GetStockObject(NULL_BRUSH); //không tô màu background của lớp static
							  }
							  else if ((HWND)lParam == g_hSumMoney)
							  {
								  SetTextColor((HDC)wParam, RGB(255, 0, 0));  
								  SetBkMode((HDC)wParam, TRANSPARENT);		
								  return (LRESULT)GetStockObject(NULL_BRUSH);
							  }

							  break;
	}
	case WM_NOTIFY:
	{
					  int wmID = LOWORD(wParam);
					  switch (wmID)
					  {
					  case ID_LISTVIEW:
						  if (((LPNMHDR)lParam)->code == NM_RCLICK)
						  {
							  int iSelect = SendMessage(g_hListView, LVM_GETNEXTITEM,-1, LVNI_FOCUSED);
						  }
						  break;
					  }
					  break;
	}
	case WM_CTLCOLORBTN:
	{
						   RECT crect;
						   HBRUSH brush;
						   HDC hdc = (HDC)wParam;
						   HWND button_handle = (HWND)lParam;

						   GetClientRect(button_handle, &crect);
						   SetBkColor(hdc, RGB(108, 184, 35));
						   SetTextColor(hdc, RGB(255, 255, 255));
						   DrawText(hdc, L"Thêm", _countof(L"Thêm") - 1, &crect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

						   brush = CreateSolidBrush(RGB(108, 184, 35));

						   return (HRESULT)brush;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_BUTTONINSERT:
		{
								WCHAR* buffer1, *buffer2, *buffer3, *buffer4;
								int len1, len2, len3, len4;
								len1=GetWindowTextLengthW(g_hCombobox);
								buffer1 = new WCHAR[len1+1];
								GetWindowText(g_hCombobox, buffer1, len1+1);
								while (buffer1[len1-1] == L' ')
								{
									buffer1[len1-1] = 0;
									len1--;
								}
								len2=GetWindowTextLengthW(g_hNote);
								buffer2 = new WCHAR[len2+1];
								GetWindowText(g_hNote, buffer2, len2+1);
								len3=GetWindowTextLengthW(g_hMoney);
								buffer3 = new WCHAR[len3+1];
								GetWindowText(g_hMoney, buffer3, len3+1);
								len4 = GetWindowTextLengthW(g_hDate);
								buffer4 = new WCHAR[len4+1];
								GetWindowText(g_hDate, buffer4, len4+1);

								if (len3 == 0)
								{
									MessageBox(hWnd, L"Bạn quên nhập số tiền", L"Thông báo", S_OK);
									delete[]buffer1;
									delete[]buffer2;
									delete[]buffer3;
									delete[]buffer4;
									break;
								}
								SetWindowText(g_hNote, L"");
								SetWindowText(g_hMoney, L"");


								InsertData(buffer1, buffer2, buffer3, buffer4);
								InsertItemToListView(buffer1, buffer2, buffer3, buffer4);
								delete[]buffer1;
								delete[]buffer2;
								delete[]buffer3;
								delete[]buffer4;

								InvalidateRect(hWnd,NULL,TRUE);
		}
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case WM_RBUTTONUP:
		{
							 POINT p;
							 p.x = GET_X_LPARAM(lParam);
							 p.y = GET_Y_LPARAM(lParam);

							 hPopupMenu = CreatePopupMenu();
							 InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_DELETEITEM, L"Xóa");
							 InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING | MF_UNCHECKED, ID_DELETEALL, L"Xóa hết");

							 ClientToScreen(hWnd, &p);

							 TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN | TPM_LAYOUTRTL, p.x, p.y, 0, hWnd, NULL);
		}
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
	{
					 
					 hdc = BeginPaint(hWnd, &ps);
					 // TODO: Add any drawing code here...
					 Graphics graphics(hdc);
					 // Create a SolidBrush object.
					 SolidBrush brush1(Color(255,60,60));
					 SolidBrush brush2(Color(121, 214, 89));
					 SolidBrush brush3(Color(255, 234, 90));
					 SolidBrush brush4(Color(64, 119, 202));
					 SolidBrush brush5(Color(178, 108, 227));
					 SolidBrush brush6(Color(114, 219, 205));
					 SolidBrush* brush[6] = { &brush1, &brush2, &brush3, &brush4, &brush5, &brush6 };

					 // Create the Rect object that defines the ellipse.
					 Rect ellipseRect1(50, 450, 10, 10);
					 Rect ellipseRect2(50, 480, 10, 10);
					 Rect ellipseRect3(50, 510, 10, 10);
					 Rect ellipseRect4(50, 540, 10, 10);
					 Rect ellipseRect5(50, 570, 10, 10);
					 Rect ellipseRect6(50, 600, 10, 10);

					 // Fill the ellipse.
					 graphics.FillEllipse(&brush1, ellipseRect1);
					 graphics.FillEllipse(&brush2, ellipseRect2);
					 graphics.FillEllipse(&brush3, ellipseRect3);
					 graphics.FillEllipse(&brush4, ellipseRect4);
					 graphics.FillEllipse(&brush5, ellipseRect5);
					 graphics.FillEllipse(&brush6, ellipseRect6);

					 if (getSumMoney() != 0)
					 {
						 int a[6] = { (float)LayTiLe(L"Ăn uống"), (float)LayTiLe(L"Di chuyển"),
							 (float)LayTiLe(L"Nhà cửa") , (float)LayTiLe(L"Xe cộ"), (float)LayTiLe(L"Nhu yếu phẩm"),
							 (float)LayTiLe(L"Dịch vụ") };
						 float temp = 0;
						 int currentColor;
						 for (int i = 0; i < 6; i++)
						 {
							 InvalidateRect(g_hNumber[i], NULL, TRUE);
							 _itow(a[i], x, 10);
							 StrCat(x, L"%");
							 SetWindowText(g_hNumber[i], x);
							 graphics.FillPie(brush[i], Rect(300, 450, 180, 180), (float)temp, a[i]*3.6);
							 if (a[i] != 0) currentColor = i;
							 temp += (float)a[i]*3.6;
							 if (temp >= 360) break;
						 }
						 if (temp < 360)
						 {
							 int x = 360 - temp;
							 graphics.FillPie(brush[currentColor], Rect(300, 450, 180, 180), temp, x);
						 }
					 }

					 //xử lý tổng tiền
					 WCHAR size[20];
					 _ltow_s(getSumMoney(), size, sizeof(size) / sizeof(WCHAR), 10);
					 SetWindowTextW(g_hSumMoney, size);

					 EndPaint(hWnd, &ps);
	}
		break;
	case WM_DESTROY:
		SaveData();
		g_vData.clear();
		GdiplusShutdown(gdiplusToken);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void init4col()
{
	// tạo cột list view
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Loại";
	lvCol.cx = 120;
	ListView_InsertColumn(g_hListView, 0, &lvCol);

	lvCol.fmt = LVCFMT_RIGHT;
	lvCol.cx = 150;
	lvCol.pszText = L"Chi tiết";
	ListView_InsertColumn(g_hListView, 1, &lvCol);

	lvCol.cx = 150;
	lvCol.pszText = L"Số tiền";
	ListView_InsertColumn(g_hListView, 2, &lvCol);

	lvCol.cx = 130;
	lvCol.pszText = L"Ngày";
	ListView_InsertColumn(g_hListView, 3, &lvCol);	
}

WCHAR* ReadStrFile(FILE *f)
{
	int i = 0;
	WCHAR* buffer = new WCHAR[100];
	do
	{
		fread(&buffer[i], 2, 1, f);
		if (buffer[i] == L'\n')
		{
			buffer[i] = 0;
			break;
		}
		i++;
	} while (!feof(f));
	return buffer;
}


void LoadData()
{
	Data *temp=new Data;
	WCHAR* buffer;
	f = _wfopen(L"config", L"rt");
	if (f == NULL) return;
	while (!feof(f))
	{
		buffer = ReadStrFile(f);
		temp->type = buffer;
		buffer = ReadStrFile(f);
		temp->note = buffer;
		buffer = ReadStrFile(f);
		temp->money = buffer;
		buffer = ReadStrFile(f);
		temp->date = buffer;
		if (wcslen(temp->type) < 4 || wcslen(temp->type)>15) return;
		InsertData(temp->type, temp->note, temp->money, temp->date);
	}
}

void InsertData(WCHAR* type, WCHAR* note, WCHAR* money, WCHAR* date)
{
	Data* temp = new Data;
	temp->type = new WCHAR[wcslen(type)];
	temp->money = new WCHAR[wcslen(money)];
	temp->note = new WCHAR[wcslen(note)];
	temp->date = new WCHAR[wcslen(date)];
	StrCpy(temp->type, type);
	StrCpy(temp->money, money);
	StrCpy(temp->note, note);
	StrCpy(temp->date, date);
	g_vData.push_back(temp);

}

void InsertItemToListView(WCHAR* type, WCHAR* note, WCHAR* money, WCHAR* date)
{
	LVITEM lv;
	lv.mask = LVIF_TEXT;
	lv.iItem = 0;
	lv.iSubItem = 0;
	lv.pszText = type;

	ListView_InsertItem(g_hListView, &lv);
	ListView_SetItemText(g_hListView, 0, 1, note);
	ListView_SetItemText(g_hListView, 0, 2, money);
	ListView_SetItemText(g_hListView, 0, 3, date);
}

void SaveData()
{
	f = _wfopen(L"config", L"wb");
	for (int i = 0; i < g_vData.size(); i++)
	{
		fwrite(g_vData[i]->type, 2, wcslen(g_vData[i]->type), f);
		fwrite(L"\n", 2, 1, f);
		fwrite(g_vData[i]->note, 2, wcslen(g_vData[i]->note), f);
		fwrite(L"\n", 2, 1, f);
		fwrite(g_vData[i]->money, 2, wcslen(g_vData[i]->money), f);
		fwrite(L"\n", 2, 1, f);
		fwrite(g_vData[i]->date, 2, wcslen(g_vData[i]->date), f);
		fwrite(L"\n", 2, 1, f);
	}
	fclose(f);
}

long getSumMoney()
{
	long sum = 0;
	for (int i = 0; i < g_vData.size(); i++)
	{
		sum += _wtol(g_vData[i]->money);
	}
	return sum;
}

int LayTiLe(WCHAR* str)
{
	long sum = 0;
	for (int i = 0; i < g_vData.size(); i++)
	{
		if (StrCmp(g_vData[i]->type, str) == 0)
		{
			sum+=_wtol(g_vData[i]->money);
		}
	}
	return (sum*100) /getSumMoney();
}