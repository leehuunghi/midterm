﻿#pragma once

#include "resource.h"
#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")
//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#pragma comment(lib, "shlwapi.lib")
#include <vector>
using namespace std;
void init4col();
WCHAR* LoaiChiTieu[6] = { L"Ăn uống", L"Di chuyển", L"Nhà cửa ", L"Xe cộ", L"Nhu yếu phẩm", L"Dịch vụ" };
HWND g_hCombobox, g_hNote, g_hMoney, g_hButtonCacul, g_hListView, g_hTitle, g_hDate, g_hSumMoney, h3;
vector<HWND> g_hNumber;
struct Data
{
	WCHAR* type;
	WCHAR* note;
	WCHAR* money;
	WCHAR* date;
	~Data()
	{
		delete[]type;
		delete[]note;
		delete[]money;
		delete[]date;
	}
};

vector<Data*> g_vData;
FILE *f;
void LoadData();
void InsertData(WCHAR* type,WCHAR* note,WCHAR* money, WCHAR* date);
void DeleteData();
void SaveData();
long getSumMoney();
int LayTiLe(WCHAR* str);
WCHAR x[4];
HMENU hPopupMenu;
void InsertItemToListView(WCHAR* type, WCHAR* note, WCHAR* money, WCHAR* date);