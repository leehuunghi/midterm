//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by quanlychitieu.rc
//
#define IDC_MYICON                      2
#define IDD_QUANLYCHITIEU_DIALOG        102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define ID_BUTTONINSERT                 106
#define IDI_QUANLYCHITIEU               107
#define IDI_SMALL                       108
#define IDC_QUANLYCHITIEU               109
#define IDR_MAINFRAME                   128
#define ID_LISTVIEW                     129
#define ID_DELETEITEM                   130
#define ID_DELETEALL                    131
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
