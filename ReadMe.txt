1. Lê Hữu Nghị - 1512349 - leehuunghi@gmail.com
2. Những gì đã làm:
	+ xây dựng chương trình quản lý chiêu tiêu theo yêu cầu
	+ Hoàn thành các yêu cầu nâng cao như vẽ biểu đồ tròn và xử lý file tiếng việt
	+ Thiết kế giao diện cho người dùng
3. Luồng sự kiện chính: Người dùng chọn loại từ combobox, nhập bất kỳ dữ liệu vào text view nào và chọn ngày sau đó nhấn thêm
4. Luồng sự kiện phụ:
	+ Người dùng nhập chữ cái vào ô tiền
	+ Người dùng chưa nhập tiền
5. Link bitbucket: https://bitbucket.org/leehuunghi/midterm
6. Link youtube: https://youtu.be/mdsqIixPYaQ